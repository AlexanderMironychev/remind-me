package com.acelost.remindme.presentation.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by alexa on 04.11.2017.
 */

public final class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        startActivity(createLaunchIntent());
        finish();
    }

    private Intent createLaunchIntent() {
        return new Intent(this, MainActivity.class);
    }
}

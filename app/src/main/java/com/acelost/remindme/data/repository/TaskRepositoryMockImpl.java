package com.acelost.remindme.data.repository;

import android.support.annotation.NonNull;

import com.acelost.remindme.data.entity.Task;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alexa on 04.11.2017.
 */

public class TaskRepositoryMockImpl implements TaskRepository {

    @NonNull
    @Override
    public List<Task> getTaskList() {
        final List<Task> taskList = new ArrayList<>();
        taskList.add(new Task("Title1", "Description1"));
        taskList.add(new Task("Title2", "Description2"));
        taskList.add(new Task("Title3", "Description3"));
        taskList.add(new Task("Title4", "Description4"));
        taskList.add(new Task("Title5", "Description5"));
        taskList.add(new Task("Title6", "Description6"));
        return taskList;
    }
}

package com.acelost.remindme.data.entity;

/**
 * Created by alexa on 04.11.2017.
 */

public class Task {

    private String mTitle;

    private String mDescription;

    public Task() { }

    public Task(String title, String description) {
        this.mTitle = title;
        this.mDescription = description;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        this.mTitle = title;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        this.mDescription = description;
    }

}

package com.acelost.remindme.data.repository;

import android.support.annotation.NonNull;
import android.support.annotation.WorkerThread;

import com.acelost.remindme.data.entity.Task;

import java.util.List;

/**
 * Created by alexa on 04.11.2017.
 */

public interface TaskRepository {

    @WorkerThread
    @NonNull
    List<Task> getTaskList();

}
